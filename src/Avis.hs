{-
Module      :  Avis
Description :  Avis's stuff
Copyright   :  (c) James Nicholls
Maintainer  :  contact@j-n.me.uk
Stability   :  experimental
-}

import System.IO (hFlush, hIsEOF, stdout, stdin)

import Avis.ObjParse

newtype Matrix = Matrix (Double, Double, Double, Double, Double, Double)

truncate' :: Int -> Double -> Double
truncate' n x = (fromIntegral (floor (x * t))) / t
    where t = 10^n

tuplify6 :: [a] -> (a,a,a,a,a,a)
tuplify6 [a,b,c,x,y,z] = (a,b,c,x,y,z)

main :: IO ()
main =
  do
    code <- readFile "./data/cube.obj"
    -- code <- readFile "./data/model.obj"
    -- code <- readFile "./data/screen.obj"
    mainloop (listGroups code) (Matrix (1, 0, 0, 1, 0, 0))

mainloop :: Group -> Matrix -> IO ()
mainloop group trans =
  do
    eof <- hIsEOF stdin
    if
      eof
    then
      return ()
    else
      do
        resp <- getLine
        group' <- ioGroup resp group
        trans' <- ioState resp trans
        mainloop group' trans' 

ioState :: [Char] -> Matrix -> IO Matrix
ioState cmd (Matrix (sx, rx, ry, sy, tx, ty)) =
  do
    let command = read cmd :: ([Char], [Double])
    mtx' <- return ( case command of
      ("translate", (dtx:dty:dtz:_)) -> [sx, rx, ry, sy, (+) tx dtx, (+) ty dty]
      ("zoom", (dz:_)) -> [(+) sx dz, rx, ry, (+) sy dz, tx, ty]
      ("rotate", (drx:dry:_)) -> [sx, (+) rx drx, (+) ry dry, sy, tx, ty]
      _ -> [sx, rx, ry, sy, tx, ty] )
    putStrLn ("{ \"group\": \"global\", \"transform\": " ++ (show mtx') ++ " }")
    hFlush stdout
    return (Matrix (tuplify6 mtx'))

ioGroup :: [Char] -> Group -> IO Group
ioGroup cmd grp =
  do
    let command = read cmd :: ([Char], [Double])
    grp' <- return ( case command of
      ("translate", (tx:ty:tz:_)) -> translateGroup (Vertex (tx, ty, tz)) grp
      ("zoom", (z:_)) -> zoomGroup z grp
      ("rotate", (rx:ry:_)) -> rotateGroup rx ry grp
      _ -> grp )
    let sorted = pointsGroup' (zSort grp')
    print (map (map (truncate' 6)) sorted)
    hFlush stdout
    return grp'

