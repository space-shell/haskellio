-- http://paulbourke.net/dataformats/obj/

{-
Module      :  ObjParse
Description :  Parses an Obj file
Copyright   :  (c) James Nicholls
Maintainer  :  contact@j-n.me.uk
Stability   :  experimental
-}


module Avis.ObjParse where

import Data.List (sortBy)

newtype Vertex = Vertex (Double, Double, Double)

newtype Vector = Vector (Vertex, Vertex)

newtype Tri = Tri (Vertex, Vertex, Vertex)

newtype Group = Group [Tri]

newtype Object = Object [Group]

--

dropTill :: ([a] -> Bool) -> [a] -> [a]
dropTill _ [] = []
dropTill query l@(x:xs)
  | query l = l
  | otherwise = dropTill query xs

takeTill :: ([a] -> Bool) -> [a] -> [a]
takeTill _ [] = []
takeTill query l@(x:xs)
 | query l = []
 | otherwise = x : takeTill query xs 

takeBetween :: ([a] -> Bool) -> ([a] -> Bool) -> [a] -> [a]
takeBetween st en list =
  takeTill en start
  where
    start = dropTill st list

stringBlock :: [Char] -> [Char] -> [Char] -> [Char]
stringBlock st en file =
  takeBetween initial ending file
    where
      initial = (\x -> (==) st (take (length st) x))
      ending = (\x -> (==) en (take (length en) x))

quickSort :: Ord a => [a] -> [a]
quickSort [] = []
quickSort (p:xs) = (quickSort lesser) ++ [p] ++ (quickSort greater)
    where
        lesser  = filter (< p) xs
        greater = filter (>= p) xs

fst' :: (a, b, c) -> a
fst' (a, b, c) = a

snd' :: (a, b, c) -> b
snd' (a, b, c) = b

trd' :: (a, b, c) -> c
trd' (a, b, c) = c

vertX :: Vertex -> Double
vertX (Vertex (x, y, z)) = x

vertY :: Vertex -> Double
vertY (Vertex (x, y, z)) = y

vertZ :: Vertex -> Double
vertZ (Vertex (x, y, z)) = z

--

vertIndexPos :: [Char] -> Int
vertIndexPos vert = (-) (read pos :: Int) 1
  where
    pos = takeWhile (/= '/') vert

-- TODO - JN - Abstract the filtering function for DRY'er code
listVerticies :: [Char] -> [Vertex]
listVerticies [] = []
listVerticies block =
  map (\(x:y:z:_) -> Vertex
    ( read x :: Double
    , read y :: Double
    , read z :: Double
    ) ) vertLineParts
  where
    fileLines = filter (not . null) (lines block)
    vertLines = filter (\x -> (==) "v " (take 2 x)) fileLines 
    vertLineParts = map (tail . words) vertLines

listTris :: [Vertex] -> [Char] -> [Tri]
listTris _ [] = []
listTris [] _ = []
listTris verts block = 
  map (\(p1:p2:p3:_) -> Tri
    ( (!!) verts (vertIndexPos p1)
    , (!!) verts (vertIndexPos p2)
    , (!!) verts (vertIndexPos p3)
    ) ) faceLineParts
  where
    fileLines = filter (not . null) (lines block)
    faceLines = filter (\x -> (==) "f " (take 2 x)) fileLines 
    faceLineParts = map (tail . words) faceLines

-- TODO - JN - A face is a collection of tris that share the same smoothing group
-- listFaces :: [Vertex] -> [Char] -> [Face]

listGroups :: [Char] -> Group
listGroups file = Group faces
  where
    verts = listVerticies file
    faces = listTris verts file

pointsVert :: Vertex -> [Double]
pointsVert (Vertex (x, y, z)) = x : y : z : []

pointsVert' :: Vertex -> [Double]
pointsVert' (Vertex (x, y, _)) = x : y : []

pointsTri :: Tri -> [Double] 
pointsTri (Tri (p1, p2, p3)) =
  pointsVert p1 ++ pointsVert p2 ++ pointsVert p3

pointsTri' :: Tri -> [Double] 
pointsTri' (Tri (p1, p2, p3)) =
  pointsVert' p1 ++ pointsVert' p2 ++ pointsVert' p3

pointsGroup :: Group -> [[Double]]
pointsGroup (Group tris) =
  map pointsTri tris

pointsGroup' :: Group -> [[Double]]
pointsGroup' (Group tris) =
  map pointsTri' tris

verticiesAdd :: Vertex -> Vertex -> Vertex
verticiesAdd (Vertex (x1, y1, z1)) (Vertex (x2, y2, z2)) =
  Vertex
    ( x1 + x2
    , y1 + y2
    , z1 + z2
    )

rotateVert :: Double -> Double -> Vertex -> Vertex -> Vertex
rotateVert theta phi (Vertex (cx, cy, cz)) (Vertex (x, y ,z)) =
  Vertex
    ( ct * dx - st * cp * dy + st * sp * dz + cx
    , st * dx + ct * cp * dy - ct * sp * dz + cy
    , sp * dy + cp * dz + cz
    )
  where
    dx = x - cx
    dy = y - cy
    dz = z - cz
    ct = cos theta
    st = sin theta
    cp = cos phi
    sp = sin phi

scaleVert :: Double -> Vertex -> Vertex -> Vertex
scaleVert delta (Vertex (cx, cy, cz)) (Vertex (x, y, z)) =
  Vertex
    ( (+) x ((/) ((-) cx x) delta)
    , (+) y ((/) ((-) cy y) delta)
    , (+) z ((/) ((-) cz z) delta)
    )

translateVertex :: Vertex -> Vertex -> Vertex
translateVertex (Vertex (tx, ty, tz)) (Vertex (x, y, z)) =
  Vertex
    ( (+) tx x
    , (+) ty y
    , (+) tz z
    )

transformVertex :: Vertex -> Vertex -> Vertex
transformVertex (Vertex (tx, ty, tz)) (Vertex (x, y, z)) =
  Vertex
    ( tx
    , ty
    , tz
    )

rotateGroup :: Double -> Double -> Group -> Group
rotateGroup theta phi g@(Group grp) = 
  Group (map transform grp)
  where
    centroid = centroidGroup g
    rv = rotateVert theta phi centroid
    transform = (\(Tri (a, b, c)) ->
      Tri
        ( rv a
        , rv b
        , rv c
        ))

centroidGroup :: Group -> Vertex
centroidGroup (Group tris) =
  Vertex
    ( (/) (vertX total) points
    , (/) (vertY total) points
    , (/) (vertZ total) points
    )
  where
    verts = foldr (\(Tri (a, b, c)) vt -> a:b:c:vt) [] tris
    total = foldr verticiesAdd (Vertex (0, 0, 0)) verts
    points = fromIntegral (length verts)

zoomGroup :: Double -> Group -> Group
zoomGroup delta grp@(Group tris) =
  Group (map transform tris)
  where
    centroid = centroidGroup grp
    transform = (\(Tri (a, b, c)) ->
      Tri
        ( scaleVert delta centroid a
        , scaleVert delta centroid b
        , scaleVert delta centroid c
        ))

translateGroup :: Vertex -> Group -> Group
translateGroup vec (Group grp) =
  Group (map transform grp)
  where
    transform = (\(Tri (a, b, c)) ->
      Tri
        ( translateVertex vec a
        , translateVertex vec b
        , translateVertex vec c
        ))

transformGroup :: Vertex -> Group -> Group
transformGroup vec (Group grp) =
  Group (map transform grp)
  where
    transform = (\(Tri (a, b, c)) ->
      Tri
        ( transformVertex vec a
        , transformVertex vec b
        , transformVertex vec c
        ))

triNormal :: Tri -> Vector
triNormal tri = undefined

triCenter :: Tri -> Vertex
triCenter (Tri
  ( (Vertex (ax, ay, az))
  , (Vertex (bx, by, bz))
  , (Vertex (cx, cy, cz))
  ) ) =
    Vertex
      ( (/) (ax + bx + cx) 3
      , (/) (ay + by + cy) 3
      , (/) (az + bz + cz) 3
      )

zSort :: Group -> Group
zSort (Group grp) =
  (Group (sortBy zDepth grp))
  where
    centerZ = vertZ . triCenter 
    zDepth tri' tri''
      | centerZ tri' > centerZ tri'' = GT
      | otherwise = LT

-- TODO - JN - Complete this mofo
cullGroup :: Vertex -> Group -> Group
cullGroup cam (Group tris) = undefined
